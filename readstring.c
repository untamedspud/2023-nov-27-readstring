#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
	char c;
	int count;
} letter_count_t;

const int USER_IN_SIZE = 1000;

void get_string_from_user( char *prompt, char b[] );
void calculate_characters( const char user_in[], letter_count_t v[], 
	int *v_active_values, const int max_v );
void print_char_results( const letter_count_t v[], const int v_active_values );
int is_letter_in_letter_count_t( const letter_count_t v[], const int v_active_values, 
	const char needle, int *found_location );

int main()
{
	char user_in[ USER_IN_SIZE ];
	letter_count_t v[100];
	int v_active_values = 0;

	get_string_from_user( "Enter a string: ", user_in );
	printf( "Processing the string \"%s\":\n", user_in );
	calculate_characters( user_in, v, &v_active_values, 100 );
	print_char_results( v, v_active_values );

	return EXIT_SUCCESS;
}

void get_string_from_user( char *prompt, char b[] )
{
	printf( "%s", prompt );
	fgets( b, USER_IN_SIZE, stdin );	
	b[ strlen( b ) - 1 ] = '\0';

	return;
}

void calculate_characters( const char user_in[], letter_count_t v[], 
	int *v_active_values, const int max_v )
{
	int i;
	int found_location;
	for( i = 0; user_in[ i ] != '\0'; i++ )
		if ( is_letter_in_letter_count_t( v, *v_active_values, user_in[ i ], &found_location ) ) {
			// if user_in[i] is already in v, then increment the count.
			(v[ found_location ].count)++;
		} else {
			// if user_in[i] is not there, add it to v with a count of 1 and increment active_values.
			
			v[ (*v_active_values) ].c = user_in[ i ];
			v[ (*v_active_values)++ ].count = 1;
		}

	return;
}

void print_char_results( const letter_count_t v[], const int v_active_values )
{
	int i;

	for ( i = 0; i < v_active_values; i++ )
		printf( "    %c:%d\n", v[i].c, v[i].count );

	return;
}

int is_letter_in_letter_count_t( const letter_count_t v[], const int v_active_values, const char needle, 
		int *found_location )
{
	int rc = 0;
	int i;

	for( i = 0; i < v_active_values; i++ )
		if ( v[ i ].c == needle ) {
			*found_location = i;
			rc = 1;
			break;
		}

	return rc; 
}
